package api

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"
	"yolobarbarians/src/utils"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/streadway/amqp"
)

var ctx = context.Background()

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const sessionKey string = "sessid"

type UserSession struct {
	IPAddress string
	LoginId   string // super placeholder for real auth, remove? Use firebase eg?
	Expires   time.Duration
}

type LoginBody struct{}

var userEvents = "user_events"

// Can you do this with corr id?
func Publish(conn *amqp.Connection, message interface{}, correlationId string) {
	ch, err := conn.Channel()
	defer ch.Close()
	utils.FailOnError(err, "Channel creation error in user api")

	err = ch.ExchangeDeclare(userEvents, "fanout", true, false, false, false, nil)
	utils.FailOnError(err, "Exchange declaration error in user api")

	body, err := json.Marshal(message)
	utils.FailOnError(err, "json marshal error before publishing")
	ch.Publish(userEvents, "", false, false, amqp.Publishing{
		ContentType:   "application/json",
		CorrelationId: correlationId,
		Body:          body,
	})
}

func Subscriber(conn *amqp.Connection) {

	ch, err := conn.Channel()
	defer ch.Close()

	err = ch.ExchangeDeclare(userEvents, "fanout", true, false, false, false, nil)
	utils.FailOnError(err, "Exchange declaration error in user api")

	q, err := ch.QueueDeclare("", false, false, true, false, nil)
	utils.FailOnError(err, "Queue declare error")

	err = ch.QueueBind(q.Name, "", userEvents, false, nil)
	utils.FailOnError(err, "Queue bind error")

	// Might not want to auto ack actually.. figure this out plz
	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	forever := make(chan bool)
	go func() {
		for d := range msgs {
			log.Printf("Lets have some callbackery going on here %s", d.Body)
		}
	}()
	<-forever
}

// placeholderish
func RandStringBytesRmndr(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}

// remove this
func Create() *gin.Engine {

	r := gin.Default()
	r.Use(cors.Default())

	// Login by logintoken (hehe...)
	r.POST("/login", func(c *gin.Context) {
		raw, _ := c.GetRawData()
		var body LoginBody
		err := json.Unmarshal(raw, &body)
		if err != nil {
			// Todo nicer, log or w/e
			c.AbortWithStatus(http.StatusBadRequest)
		}

		// Let's pretend we do some db check here.
		id := RandStringBytesRmndr(10)

		expires := time.Hour * 24 * 7
		Cache.Set(ctx, id, &UserSession{
			// LoginId:   body.LoginID, // Lets put some other relevant stuff here..
			IPAddress: c.Request.RemoteAddr,
			Expires:   expires,
		}, expires)
		// Very trivial placeholderish
		c.SetCookie(sessionKey, id, int(expires)*60*60, "/login", "localhost", false, true)

		c.JSON(http.StatusCreated, gin.H{sessionKey: id})
	})

	// responseMap = make(map[string]func())

	r.POST("/register")

	return r
}

func SignupHandler(c *gin.Context) {

}

func loginWithCredentialsHandler(c *gin.Context) {}

func LoginWithTokenHandler(c *gin.Context) {
	// Login by session id cookie
	// Get the id cookie
	id, err := c.Cookie(sessionKey)
	fmt.Println("cookie", id)
	// Login :D
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
	} else {
		_, err := Cache.Get(ctx, id).Result()
		if err == redis.Nil {
			c.SetCookie(sessionKey, "", -1, "/login", "localhost", false, true)
			c.AbortWithStatus(http.StatusUnauthorized)
		} else if err != nil {
			c.SetCookie(sessionKey, "", -1, "/login", "localhost", false, true)
			c.AbortWithStatus(http.StatusUnauthorized)
		} else {
			c.JSON(200, gin.H{})

		}
	}
}

func RegisterAccountHandlers(g *gin.RouterGroup) {
	g.GET("/login", LoginWithTokenHandler)
	g.POST("/login", loginWithCredentialsHandler)
	g.POST("/signup", SignupHandler)
}
