package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
)

var Cache *redis.Client

type Api struct {
}

func (api *Api) Init() {
	// Start redis
	Cache = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	// Start gin
	r := gin.Default()
	r.Use(cors.Default())
	v1 := r.Group("/api/v1")
	RegisterAccountHandlers(v1)

	r.Run(":8080")
}
