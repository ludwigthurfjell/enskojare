package mongodb

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoClient struct {
	Client *mongo.Client
	Name   string
	Ctx    context.Context
}

func Connect(name string) (*MongoClient, error) {
	clientOptions := options.Client().ApplyURI("mongodb://0.0.0.0:27017/skojarensmongodb")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return nil, err
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}
	// defer func() {
	// 	if err = Database.Client.Disconnect(context.TODO()); err != nil {
	// 		panic(err)
	// 	}
	// }()
	return &MongoClient{
		Client: client,
		Ctx:    context.TODO(),
		Name:   name,
	}, nil
}
