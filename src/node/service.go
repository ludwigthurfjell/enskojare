package node

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/streadway/amqp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

/*
	PartyEnters
	PartyLeaves

*/

type NodeUser struct {
	Id       string `bson:"userId"`
	FullName string `bson:"name"`
}

type Market struct{}

/**

 */
type Party struct {
	Type        string
	OwnerUserId string
	UserIds     []string
}

type Node struct {
	Id   string `bson:"_id,omitempty"`
	Name string `bson:"name"`

	Users []NodeUser `bson:"users"`
}

type NodeEvent struct{}

func errorPanic(err error, stuff string) {
	if err != nil {
		fmt.Println(stuff)
		panic(err)
	}
}

// Alt use redis actually
var instance Node

const dbName = "ludzgame_nodesdb"

// Might want to pass pointers instead, but if these are to be ran independantly in containers, they
// kind of need to initialize this stuff by themselves anyway...
func Run(nodeId string, mongoUrl string, amqpUrl string) {
	ojebctId, err := primitive.ObjectIDFromHex(nodeId)
	if err != nil {
		panic(err)
	}
	// clientOptions := options.Client().ApplyURI("mongodb://0.0.0.0:27017/settlementdb")
	clientOptions := options.Client().ApplyURI(mongoUrl + "/" + dbName)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	// Load from db by id, and set instance
	collection := client.Database(dbName).Collection("settlements")
	collection.FindOne(context.TODO(), bson.M{"_id": ojebctId}).Decode(instance)

	// Listen to specific channel events from message broker
	conn, err := amqp.Dial(amqpUrl)
	defer conn.Close()
	errorPanic(err, "Error connecting to amqp")

	consumeChannel, err := conn.Channel()
	defer consumeChannel.Close()
	errorPanic(err, "Channel error")

	err = consumeChannel.ExchangeDeclare("NodeSubExchange", "fanout", true, false, false, false, nil)
	errorPanic(err, "Exchange error")

	consumeQueue, err := consumeChannel.QueueDeclare(nodeId, false, false, true, false, nil)
	errorPanic(err, "Consume queue error")

	// 2nd param key should probably be an event identifyer?
	err = consumeChannel.QueueBind(consumeQueue.Name, "", "NodeSubExchange", false, nil)
	errorPanic(err, "Queue bind error")

	consumeMsgs, err := consumeChannel.Consume(consumeQueue.Name, "", true, false, false, false, nil)

	forever := make(chan (bool))
	go func() {
		for m := range consumeMsgs {
			var event NodeEvent
			json.Unmarshal(m.Body, &event)
			// Party entering

		}
	}()
	<-forever
}
