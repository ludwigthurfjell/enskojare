package user

import (
	"encoding/json"
	"fmt"
	mongodb "yolobarbarians/cmd/mongoDb"

	"github.com/streadway/amqp"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var MongoClient *mongodb.MongoClient

type User struct {
	UserID     string
	Firstname  string
	Lastname   string
	LocationID string
}

func (u *User) Insert() {

}

const (
	UserCreateEventType int    = 1
	UserGetEventType    int    = 2
	UserQueueRequest    string = "UserQueueRequest"
	UserQueuePublish    string = "UserQueuePublish"
	UserExchange        string = "UserExchange"
)

type UserEvent struct {
	Type int
	Data interface{}
}

func register(conn *amqp.Connection) {
	ch, err := conn.Channel()
	if err != nil {
		panic(err)
	}
	defer ch.Close()
	err = ch.ExchangeDeclare(UserExchange, "fanout", true, false, false, false, nil)
	if err != nil {
		panic(err)
	}
	q, err := ch.QueueDeclare("", false, false, true, false, nil)
	if err != nil {
		panic(err)
	}

	err = ch.QueueBind(q.Name, "", UserExchange, false, nil)
	if err != nil {
		panic(err)
	}
	// Inte auto ack?
	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		panic(err)
	}

	forever := make(chan (bool))

	// Listen forever
	go func() {
		for m := range msgs {
			var user User
			json.Unmarshal(m.Body, &user)

			// Insert in db

			res, err := MongoClient.Client.Database(MongoClient.Name).Collection("users").InsertOne(MongoClient.Ctx, user)
			if err != nil {
				// do something
				fmt.Println("Fel i mongo skapande", err)
			}

			// Publish
			err = ch.Publish(UserExchange, "", false, false, amqp.Publishing{CorrelationId: m.CorrelationId, ContentType: "text/plain", Body: []byte(res.InsertedID.(primitive.ObjectID).Hex())})
			if err != nil {
				fmt.Println("publish register error", err)
			}
		}
	}()

	<-forever
}

func login() {

}

// remove
func UserService() {
	// Need us some rabbitmq hustle
	rmqConn, err := amqp.Dial("amqp://localhost")
	if err != nil {
		panic(err)
	}
	defer rmqConn.Close()

	// Need us some DB stuffle
	MongoClient, err = mongodb.Connect("skojarens")
	if err != nil {
		panic(err)
	}
	fmt.Println("Connected to mongo db")

	// All the listeners
	register(rmqConn)
}
