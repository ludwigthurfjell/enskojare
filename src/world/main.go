package world

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// Might use some internal message brokering for this monolith, I just want some stuff right now
// without caring about any huge scale architecture.

var World *WorldDb

func Create() *gin.Engine {
	World = Yolo(15)

	r := gin.Default()

	r.Use(cors.Default())

	r.GET("/world", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"settlements": World.Settlements,
			"edges":       World.Edges,
		})
	})
	return r
	// r.Run(":1337")
}
