package world

import (
	"math"
	"math/rand"
	"strconv"
	"time"

	"github.com/fogleman/delaunay"
)

type Settlement struct {
	ID    int     `json:"id" bson:"id"`
	Name  string  `json:"name" bson:"name"`
	Point Point2d `json:"point" bson:"point"`
}

type Edge struct {
	ID     int     `json:"id" bson:"id"`
	AId    int     `json:"settlementAId" bson:"settlementAId"`
	APoint Point2d `json:"pointA" bson:"pointA"`
	BPoint Point2d `json:"pointB" bson:"pointB"`
	BId    int     `json:"settlementBId" bson:"settlementBId"`

	Distance int
}

type Segment2d struct {
	A Point2d
	b Point2d
}

type Point2d struct {
	X int `json:"x" bson:"x"`
	Y int `json:"y" bson:"y"`
}

func (c *Point2d) distanceTo(t *Point2d) int {
	dxsq := math.Pow(float64(t.X-c.X), 2)
	dysq := math.Pow(float64(t.Y-c.Y), 2)

	return int(math.Round(math.Sqrt(dxsq + dysq)))
}

func (db *WorldDb) edgeExists(s1 *Settlement, s2 *Settlement) bool {
	r := false
	for _, e := range db.Edges {
		if e.AId == s1.ID && e.BId == s2.ID || e.AId == s2.ID && e.BId == s1.ID {
			r = true
			break
		}
	}
	return r
}

type WorldDb struct {
	Settlements []Settlement
	Edges       []Edge
}

func InitDb() *WorldDb {
	return &WorldDb{
		Settlements: make([]Settlement, 0),
		Edges:       make([]Edge, 0),
	}
}

func (db *WorldDb) InsertSettlement(s *Settlement) {
	s.ID = rand.Intn(50000)
	db.Settlements = append(db.Settlements, *s)
}

func (db *WorldDb) AddEdge(e *Edge) {
	e.ID = rand.Intn(50000)
	db.Edges = append(db.Edges, *e)
}

func randRange(min, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min+1) + min
}

func nextHalfedge(e int) int {
	var r int
	if e%3 == 2 {
		r = e - 2
	} else {
		r = e + 1
	}
	return r
}
func prevHalfedge(e int) int {
	var r int
	if e%3 == 0 {
		r = e + 2
	} else {
		r = e - 1
	}
	return r
}

func Yolo(size int) *WorldDb {
	db := InitDb()
	var points []delaunay.Point
	for i := 0; i < size; i++ {
		dpoint := delaunay.Point{X: float64(rand.Intn(1900)), Y: float64(rand.Intn(1000))}
		nSettlement := Settlement{
			Name:  "Settlement" + strconv.Itoa(i+1),
			Point: Point2d{X: int(dpoint.X), Y: int(dpoint.Y)},
		}
		db.InsertSettlement(&nSettlement)
		points = append(points, dpoint)
	}
	tr, errs := delaunay.Triangulate(points)
	if errs != nil {
		panic(errs)
	}

	for e := 0; e < len(tr.Triangles); e++ {
		if e > tr.Halfedges[e] {
			p := points[tr.Triangles[e]]
			q := points[tr.Triangles[nextHalfedge(e)]]
			edge := &Edge{
				APoint: Point2d{X: int(p.X), Y: int(p.Y)},
				BPoint: Point2d{X: int(q.X), Y: int(q.Y)},
			}
			edge.Distance = edge.APoint.distanceTo(&edge.BPoint)
			db.AddEdge(edge)
		}
	}
	return db
}
