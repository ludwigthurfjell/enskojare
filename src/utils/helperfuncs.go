package utils

import "log"

// Super great naming practice :D :D :D :D

// Logs and fatals any error
func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
