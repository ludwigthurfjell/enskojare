package main

import (
	"fmt"
	"sync"
	"yolobarbarians/src/world"
)

func main() {
	wg := new(sync.WaitGroup)
	wg.Add(2)
	fmt.Println("Kör vi main nu eller?")
	// userEngine := user.Create()
	worldEngine := world.Create()
	go func() {
		// userEngine.Run(":1337")
		wg.Done()
	}()

	go func() {
		worldEngine.Run(":1338")
		wg.Done()
	}()

	wg.Wait()
}
